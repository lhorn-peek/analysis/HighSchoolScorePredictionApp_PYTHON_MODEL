from azureml import services
@services.publish('YOUR AZURE ML WORKSPACE ID', 'AZURE ML API KEY')
@services.types(peer_indx=float, num_test_takers=int, env_cat_score=float, school_wide_pup_teach_ration=float, perf_cat_score=float, num_classes_mean=float, size_smallest_class=float, tot_register_mean=float, prog_cat_score=float, avg_class_size_mean=float)
@services.returns(list) #0, or 1, or 2
def predictSchoolSats(peer_indx, num_test_takers, env_cat_score, school_wide_pup_teach_ration, perf_cat_score, num_classes_mean, size_smallest_class, tot_register_mean, prog_cat_score, avg_class_size_mean):
 inputArray = [peer_indx, num_test_takers, env_cat_score, school_wide_pup_teach_ration, perf_cat_score, num_classes_mean, size_smallest_class, tot_register_mean, prog_cat_score, avg_class_size_mean]
 return model.predict(inputArray).tolist()


# In[4]:

dir(predictSchoolSats)


# In[5]:
#call the webservice to predict
predictSchoolSats.service(2.24,60,5.2,13.2,11.4,3.709677419,18.87096774,81.87096774,31.2,22.90645161)


# In[6]:

predictSchoolSats.service.help_url


# In[7]:

predictSchoolSats.service.url


# In[8]:

predictSchoolSats.service.api_key