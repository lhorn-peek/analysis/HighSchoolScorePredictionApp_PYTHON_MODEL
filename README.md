# HighSchoolScorePredictionApp_PYTHON_MODEL

Python Machine Learning Scripts using SkiLearn ML Library. 

These scripts are used to:

1) Clean and restructure the learning data.

2) Select the most important input variables for the model.

3) Create the Machine Learning Model.

4) Host the model as a webservice in AZURE.

The prediction web service is used in the repository 'HighSchoolScorePredictionApp'
