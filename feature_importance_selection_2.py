
# coding: utf-8

# In[49]:

import pandas as pd

# Load the data
train = pd.read_csv('C:/data/NYC_Schools/train.csv')
target = pd.read_csv('C:/data/NYC_Schools/target.csv')

train = train.set_index('DBN')
target = target.set_index('DBN')


# In[50]:

train[:3]


# In[51]:



train = train.set_index('DBN')
target = target.set_index('DBN')

#target = target.drop(['DBN','Writing Mean','Mathematics Mean'],axis=1)
#train = train.drop(['DBN'],axis=1)


# In[47]:

#train[:3]
#target[:3]


# In[52]:

from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import Imputer
import numpy as np
import math


trainFinal = Imputer().fit_transform(train)

model = RandomForestRegressor(n_estimators=100, n_jobs=-1)
model.fit(trainFinal, target)

predictions = np.array(model.predict(trainFinal))
rmse = math.sqrt(np.mean((np.array(target.values) - predictions)**2))
imp = sorted(zip(train.columns, model.feature_importances_), key=lambda tup: tup[1], reverse=True)

print "RMSE: " + str(rmse)
print "10 Most Important Variables:" + str(imp[:10])


# In[53]:

train.filter(['PEER INDEX*','Number of Test Takers','2009-2010 ENVIRONMENT CATEGORY SCORE','SCHOOLWIDE PUPIL-TEACHER RATIO','2009-2010 PERFORMANCE CATEGORY SCORE','NUMBER OF CLASSES.mean','SIZE OF SMALLEST CLASS.mean','TOTAL REGISTER.mean','2009-2010 PROGRESS CATEGORY SCORE','AVERAGE CLASS SIZE.mean']).to_csv('C:/data/NYC_Schools/train_imp_features.csv')


# In[54]:

train.columns


# In[56]:

trainFinal

#trainFinal.filter(['PEER INDEX*','Number of Test Takers','2009-2010 ENVIRONMENT CATEGORY SCORE','SCHOOLWIDE PUPIL-TEACHER RATIO','2009-2010 PERFORMANCE CATEGORY SCORE','NUMBER OF CLASSES.mean','SIZE OF SMALLEST CLASS.mean','TOTAL REGISTER.mean','2009-2010 PROGRESS CATEGORY SCORE','AVERAGE CLASS SIZE.mean']).to_csv('C:/data/NYC_Schools/train_imp_features_final.csv')


# In[57]:

pd.DataFrame(trainFinal)


# In[ ]:



