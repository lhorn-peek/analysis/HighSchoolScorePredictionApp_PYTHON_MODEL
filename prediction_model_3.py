
# coding: utf-8

# In[ ]:




# In[1]:

import pandas as pd
from sklearn.preprocessing import Imputer

# Load the data
train = pd.read_csv('C:/data/NYC_Schools/train_imp_features.csv')
target = pd.read_csv('C:/data/NYC_Schools/target.csv')

train = train.set_index('DBN')
target = target.set_index('DBN')

# REMEMBER to: trainFinal = Imputer().fit_transform(train) 
trainFinal = Imputer().fit_transform(train)

# Linear Regression
import numpy as np

#from sklearn.linear_model import LinearRegression
#from sklearn.neighbors import KNeighborsRegressor
#from sklearn.tree import DecisionTreeRegressor
#from sklearn.linear_model import Ridge
from sklearn.ensemble import RandomForestRegressor
from sklearn import cross_validation

# fit a regression model to the data
#model = LinearRegression()
#model = KNeighborsRegressor()
#model = DecisionTreeRegressor()
#model = Ridge(alpha=0.1)
model = RandomForestRegressor()


X_train, X_test, y_train, y_test = cross_validation.train_test_split(trainFinal, target, test_size=0.3, random_state=0)

model.fit(X_train, y_train)

print(model.score(X_test, y_test))

#model.predict([2.24,60,5.2,13.2,11.4,3.709677419,18.87096774,81.87096774,31.2,22.90645161])

#print(model)
# make predictions
#expected = target
#predicted = model.predict(trainFinal)
# summarize the fit of the model
#mse = np.mean((predicted-expected)**2)
#print(mse)
#print(model.score(trainFinal, target))


# In[45]:

#train.dtypes


# In[2]:

model.predict([2.24,60,5.2,13.2,11.4,3.709677419,18.87096774,81.87096774,31.2,22.90645161]).tolist()





